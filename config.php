<?php 
/**
 * Contains basic constants
 *
 * @package WordPress
 */

# YH-VC Participants WLM level id 
# define('YH_VC_PARTICIPANTS_LEVEL', 1263956352); // EDITED BY SS 20120112 (From old events.YH site)
define('YH_VC_PARTICIPANTS_LEVEL', 1326399508); // New SKU for WLM for new Level "YHVC-Participants"

# YH-VC Participants dashboard page 
# October 28, 2010 (YH-VC 2010 was set to the '/lobby' but I've moved it to the sales letter now.)
define('YH_VC_PARTICIPANTS_DASHBOARD', '/2012/register'); // post event
// define('YH_VC_PARTICIPANTS_DASHBOARD', '/2012-preview'); // pre event

# Event start time
# define('YH_EVENT_START_TIME', mktime(0, 0, 0, 2, 19, 2010)); // Feb 19, 2010
define('YH_EVENT_START_TIME', mktime(0, 0, 0, 2, 8, 2011)); // Feb 8, 2011