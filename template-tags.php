<?php

# Returns the first tab menu html for user with YH-VC Participants level
# or false otherwise
function yhvc_participant_menu_tab1()
{	
	global $current_user;
	
	// Check whether user is logged in	
	if (!is_user_logged_in()) 
		return false;
	
	$tab1Html = '<li><a href="lobby" style="background:orange">Lobby</a></li>';		
		
	return (userHasWlmLevel($current_user->ID, YH_VC_PARTICIPANTS_LEVEL)) ? 
		$tab1Html : false; 
}

function eventsCustomizationEncodeUrl($url)
{
	return str_replace(array('/', '+'), array('-', '_'),
		base64_encode($url));
}

# Outputs "Event Countdown" box before start event
# and "Event in session" box after start event (depending on user logged in)
function showEventStateBox()
{
	$curTheme = get_theme_root_uri() . '/' . get_current_theme();

	if (false && (time() < YH_EVENT_START_TIME)) // before start event
		echo <<<EOF
<table width="221" height="106" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>		
<td background="$curTheme/images/countdown.png">
<div id="cnt">
<script language="JavaScript">
	TargetDate = "2/19/2010 00:00 AM";
	BackColor = "transparent";
	ForeColor = "#ffffff";
	CountActive = true;
	CountStepper = -1;
	LeadingZero = true;
	DisplayFormat = "%%D%% Days %%H%%h %%M%%m %%S%%s";
	FinishMessage = "It is finally here!";
</script>
	
<script language="JavaScript" src="$curTheme/countdown.js"></script>
</div>
</td>
</tr>
</table>
EOF;
	else
	{
		if (is_user_logged_in())
		{
			$tableHeight= '66px';
			$bgImg = 'countdown3.png';
			$loginButton = '';
		}
		else
		{
			// Prepare login link
			$thisPage = site_url() . @$_SERVER['REQUEST_URI'];
			if (strpos(@$_SERVER['REQUEST_URI'], 'denied?') !== false
				&& ($wlfrom = @$_GET['wlfrom']))
				$afterLogin = site_url() . @$_GET['wlfrom'];
			else
				$afterLogin = $thisPage;
			$redirAfterLogin = eventsCustomizationEncodeUrl($afterLogin);
			
			$tableHeight= '106px';
			$bgImg = 'session.png';
			$profilesUrl = get_option('yh_profilesUrl');
			$loginButton = "<a href=\"$profilesUrl/login/0/$redirAfterLogin\">
				<img src=\"$curTheme/images/btnlogin.png\"/></a>";
		}
		echo <<<EOF
<table width="221" height="$tableHeight" border="0" 
	align="center" cellpadding="0" cellspacing="0">
<tr>			
		<td background="$curTheme/images/$bgImg" valign="bottom">
			$loginButton
		</td>
</tr>
</table>
EOF;
		
	}
}

?>