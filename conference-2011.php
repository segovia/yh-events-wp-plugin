<?php

function yhvc_virtual_classroom_2011($sessionKey, $forumDestination, $conferenceVars)
{
	global $current_user;
	global $yh_interactor;
	
	extract($conferenceVars);
?>
	
<div id="spktb"> <p><span class="wktitle"><?php echo $SessTitleL ?></span>  <br />
  <br />
  <strong>Category:</strong> <?php echo $SessCat ?></p>

  <table width="97%" height="36" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="middle" background="http://events.yogahub.com/wp-content/themes/yogahub/images/callbox2.png"><div class="callbox1">
        <div class="date"><?php echo $SessDate ?> from <?php echo $SessTime ?></div>
      </div></td>
      <td width="42" valign="top"><a href="http://events.yogahub.com/help"><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/callbox3.png" width="42" height="36" border="0" /></a></td>
    </tr>
  </table>
  <div id="callbox"><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/stepA1.png" />
    <p>&nbsp;</p>
      <p><?php echo $SessPrep ?></p>
      <p>&nbsp;</p>

<?php if ($HandoutURL1 != "") { // Show Primary Handouts ?>
      <p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-pdf.png" width="25" height="25" border="0" align="absmiddle" /> <a href="<?php echo $HandoutURL1 ?>" target="_blank"><?php echo $HandoutTxt1 ?></a></p>
<?php } // END IF Statement - Primary Handout ?>

<?php
if ($HandoutURL2 != "") { // If there is more than 1 Handout
$HandCounter = 2;

echo "<p><ul>";
while ( $HandCounter <= $NumHand ) {
	echo "<img src=\"http://affiliates.yogahub.com/images/ico_pdf.png\" hspace=\"5\" width=\"12\"><a href=\"";
	echo ${'HandoutURL' . $HandCounter};
	echo "\" target=\"_blank\">";
	echo ${'HandoutTxt' . $HandCounter};
	echo "</a><br />";
	$HandCounter = $HandCounter + 1;
}
echo "</ul></p><p>&nbsp;</p>";

} // End if - Speaker Products
?>

</div>

<?php // SET TIME AND DATE FOR *END* OF LIVE CALL + 5 MIN
if (time() < $CallEndTime)
{ ?>


<div id="callbox2">
    <p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/stepA2.png" /></p>
    <p>&nbsp;</p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="190" valign="top"><p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-dialin.png" /></p>
      <p>&nbsp;<br />
        Call: <strong><?php echo $DialNum ?></strong><br />
        Conf. ID: <strong><?php echo $PinCode ?></strong><br />
        <a href="http://events.yogahub.com/local" target="_blank">View Local Phone Numbers</a><br />
        <a href="skype:joinconference?call" class="skypenow"><img src="http://www.webbestdesigner.com/images/skype-icon.gif" width="14"></a> or <a href="skype:joinconference?call" class="skypenow">Connect Via Skype</a> | <a href="http://instantteleseminar.com/Skype" target="_blank">Help</a></p></td>
    <td valign="top"><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-OR.png" /></td>
    <td valign="top"><p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-stream.png" /></p>
     <div align="center"> <p class="alert">Available 5 minutes before the call</p>
      <p >

<?php // HIDE STREAMING PLAYER
if (time() < $NearStart)
{ ?>
<font color="FF0000"><b>Please Check Back Later</b></font><p class="alert">Note: You must refresh the page.</p>
<?php } else { // SHOW STREAMING PLAYER ?>

<?php if($browser == 'iphone'){ // iPhone (iOS) Compatible HTML5 Player ?>
<audio controls preload="auto" autobuffer>Unsupported element.
  <source src="http://webcast.nfinite.com/<?php echo $StreamID ?>.mp3?StreamLive&PM=false&songVolume=100" />
</audio>
<?php } else { // Show iFrame Flash Audio Player ?>
<iframe width="180px" class="audbox" height="45px" frameborder="0" scrolling="no" src="http://www.yourconferenceline.com/members/WebPresenter/Play.asp?stream=<?php echo $StreamID ?>&amp;preffix=custom&amp;x=<?php echo $EventID ?>"></iframe>
<?php } // END iPhone/iOS Player ?>

</p>
      <p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-alert.png" />
<?php } // END IF Statement to display LIVE vs. REPLAY page info ?>

</p></div>
</td>
  </tr>
</table>
  </div>
  <div id="callbox3">  
     <p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/stepA3.png" /></p>
     <p>&nbsp;</p>
     <p><?php yhqa_display_form($sessionKey, $forumDestination); ?></p>    
  </div>
 
<?php if ($PowerPoint != "") { // Show PowerPoint Slides ?> 

<div id="callbox3">
    <p><strong>Step 4: </strong>View PowerPoint Slides Below</p>
    <p>&nbsp;</p>
    <p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-ppt.png" align="left" /><strong><a href="javascript:toggle2('e200x200','myHeader8');" class="bannerlnk" id="myHeader8" >Click here to load the slides</a></strong><br />You may still choose to listen by phone, skype or via the streaming web player found inside the slide presentation window.</p>
    <p>&nbsp;</p>
            
  <div id="e200x200" style="display: none;">
    <iframe style="text-align: left; vertical-align: top;" src="http://events.yogahub.com/wp-content/themes/yogahub/ppt-imglive.php?EventID=<?php echo $EventID ?>" width="650px" height="530px" frameborder="0" scrolling="no"></iframe>
  </div>
</div>

<?php } // End Power Point ?> 
 
<?php } else { // DO THIS AFTER TIME SET FROM ABOVE EXPIRES ?>

<div id="callbox3">
    <p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/stepB2.png" /></p>
    <p>&nbsp;</p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="45"><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-spk.png" />  </td>
        <td width="100">

<?php if ($AAudioID == "") { // Show Nconnects Instant Replay ?> 

	<iframe id="RecordingPlayer1" frameborder="0" height="25px" width="75px" align="left" scrolling="no" src="http://www.attendthisevent.com/Modern/Player.asp?fn=<?php echo $StreamID ?>-001&amp;pm=1&amp;w=75&amp;h=25&amp;bgc=FFFFFF"></iframe>

<?php } else { // Show AudioAcrobat Player ?>

	<iframe src="http://www.audioacrobat.com/playweb?audioid=<?php echo $AAudioID ?>&amp;buffer=5&amp;shape=6&amp;fc=c2e2af&amp;pc=90c870&amp;kc=00A000&amp;bc=FFFFFF&amp;brand=1&amp;player=ap29" width="150" frameborder="0" height="40" scrolling="no"></iframe>

<?php } // End IF - Audio Player ?> 

<p>&nbsp;</p></td>
        <td><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-alert.png" /></td>
      </tr>
    </table>
  </div>
  <div id="callbox3"><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/stepB3.png" />
    <p>&nbsp;</p>
       <p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-ques.png" align="left" /><strong><a href="<?php echo $ForumURL ?>">Speaker Followup Questions</a></strong><br />
         Each speaker has an area for further questions and discussions related to their session or workshop. </p>
  </div>

<?php if ($PowerPoint != "") { // Show PowerPoint Slides ?> 
       <p>&nbsp;</p>
  <div id="callbox3">
    <p><strong>Step 4: </strong>View PowerPoint Slide Presentation</p>
 <p>&nbsp;</p>
    <p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-ppt.png" align="left" /><strong><a href="javascript:toggle2('e200x200','myHeader8');" class="bannerlnk" id="myHeader8" >Click here to load the slides</a></strong><br />
        You may listen to just the audio replay above, or via the streaming web player found inside the slide presentation window. </p>
 <p>&nbsp;</p>
  </div>
     
  <div id="e200x200" style="display: none;">
  <iframe style="text-align: left; vertical-align: top;" src="http://events.yogahub.com/wp-content/themes/yogahub/ppt-img.php?EventID=<?php echo $EventID ?>&StreamID=<?php echo $StreamID ?>" width="650px" height="530px" frameborder="0" scrolling="no"></iframe>
  </div>

<?php } // End Power Point ?>

<?php } 
// ===================================
// END IF Statement to display LIVE vs. REPLAY page info 
?>

<?php
if ($ProdTitle1 != "") { // Speaker Products
$counter = 1;

echo "&nbsp;<br /><div id=\"callbox3\">";
echo "<p><strong>Recommended Products</strong></p><p>&nbsp;</p><table border=\"0\" cellspacing=\"0\" cellpadding=\"4\"><tr>";

while ( $counter <= $NumProd ) {
	echo "<td valign=\"top\"><div align=\"center\"> <a href=\"";
	echo ${'ProdURL' . $counter};
	echo "\"><img src=\"";
	echo ${'ProdImg' . $counter};
	echo "\" height=\"150\" border=\"0\" /></a><br /></div><br />";
	echo "<div align=\"center\"><div id=\"priceboxsm\"><div class=\"price\">";	
	echo ${'ProdPrice' . $counter};
	echo "</div></div></div><br />";
	echo "<div align=\"center\"><a href=\"";
	echo ${'ProdURL' . $counter};	
	echo "\">";
	echo ${'ProdTitle' . $counter};	
	echo "</a></div></td>";

	$counter = $counter + 1;
}
echo "</tr></table>";
echo "</div><p>&nbsp;</p>";

} // End if - Speaker Products
?>

  
  
</div>
  <table width="197" border="0" cellpadding="0" cellspacing="0" >
   <tr>
     <td valign="top" background="http://events.yogahub.com/wp-content/themes/yogahub/images/thumbnail1.gif">&nbsp;</td>
     <td height="161" valign="top" background="http://events.yogahub.com/wp-content/themes/yogahub/images/thumbnail1.gif"><div id="spkimg"><img src="<?php echo $AvatarImg ?>" /></div></td>
   </tr>
   <tr>
     <td valign="top" background="http://events.yogahub.com/wp-content/themes/yogahub/images/thumbnail2.gif">&nbsp;</td>
     <td height="74" valign="top" background="http://events.yogahub.com/wp-content/themes/yogahub/images/thumbnail2.gif"><div id="spkcon"><a href="http://www.myyogahub.com/<?php echo $MyhID ?>"><img src="http://events.yogahub.com/wp-content/themes/yogahub/speakers/spkcontact.gif" border="0" /></a></div></td>
   </tr>
<tr>
  <td valign="top">&nbsp;</td>
     <td valign="top"><div class="misclnk">
       <p><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-lotus.png" align="absmiddle" /><a href="http://events.yogahub.com/schedule">Event Schedule</a><br />
         <img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-lotus.png" border="0" align="absmiddle" /><a href="http://events.yogahub.com/<?php echo $PageSlug ?>-2011">Session Outline</a><br />
         <img src="http://events.yogahub.com/wp-content/themes/yogahub/images/icon-lotus.png" align="absmiddle" /><a href="http://events.yogahub.com/<?php echo $PageSlug ?>-bio">Speaker Bio<br />
</a></p>
       </div></td>
</tr>
<tr>
  <td valign="top">&nbsp;</td>
  <td valign="top">&nbsp;</td>
</tr>

<?php if (time() < $CallEndTime) { ?>
<tr>
  <td valign="top">&nbsp;</td>
  <td valign="top"><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/infobox.png" border="0" class="infotop" /></td>
</tr>
<tr>
  <td valign="top">&nbsp;</td>
  <td valign="top">
       <div class="infobox"> 
      <p><?php if (time() < $CallStartTime) {print "Conference Call Starts In:";} else {print "Conference Call Ends In:";} ?><br />
      <strong><span id="countdown1"><?php if (time() < $CallStartTime) echo $CallStart; else echo $CallEnd; ?></span></strong>      <br />
      <br />Current PST Time: <strong><br />
	<span id="timecontainer2"></span></strong><br />
        <a href="http://events.yogahub.com/time-zones">View Time Zones</a></p>
      <p><?php include("wp-content/themes/yogahub/live-info.php"); ?></p>
       </div></td>
</tr>
<?php } ?>
<tr>
  <td valign="top">&nbsp;</td>
  <td valign="top">&nbsp;</td>
</tr>
<tr>
  <td valign="top">&nbsp;</td>
  <td valign="top"><?php include("wp-content/themes/yogahub/live-info2.php"); ?></td>
</tr>
 </table>

<script type="text/javascript">
new showLocalTime("timecontainer2", "server-php", -180, "short")
</script>

<div class="clear"> </div>
<div id="callbox3"><strong>Event Sponsors</strong><br /><a href="http://events.yogahub.com/sponsors"><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/2011spons2.jpg" border="0" /><br /><img src="http://events.yogahub.com/wp-content/themes/yogahub/images/2011spons1.jpg" border="0" /></a></div>

<?php
}