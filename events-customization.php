<?php 
/*
Plugin Name: Events Customization
Plugin URI: http://www.yogahub.com
Description: Customizes YH Events site
Version: 1.0
Author: XLR8: Anton Andriyevskyy, Oleg Kravchenko
*/

# Some basic constants
require_once(dirname(__FILE__) . '/config.php');

# Add handlers
require_once(dirname(__FILE__).'/handlers.php');

# Inlude functions for templates
require_once(dirname(__FILE__).'/template-tags.php');

# Conference templates
require_once(dirname(__FILE__).'/conference-2011.php');

require_once dirname(__FILE__).'/EventDateParser.php';