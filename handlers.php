<?php

# Handle the case when user not logged in
add_action('init', 'yhvc_participant_redirect', 1);

/**
 * Checks whether user has wlm level
 * @param $userId int WordPress user id
 * @param $wlmLevelId int WLM level id
 * @return bool
 */
function userHasWlmLevel($userId, $wlmLevelId)
{
	// Check in wp_usermeta table
	$userLevels = get_usermeta($userId, 'wpm_leveltimestamp');
	if (is_array($userLevels) && 
		array_key_exists($wlmLevelId, $userLevels))	
		return true;
		
	// Check in WLM Options
	$WlmOptions = get_option('WishListMemberOptions');
	if (!$WlmOptions) 
		return false;
	$WlmOptions = unserialize($WlmOptions);
	if (!array_key_exists('Members', $WlmOptions)) 
		return false;
	$WlmOptionsMembers = $WlmOptions['Members'];
	if (!array_key_exists($wlmLevelId, $WlmOptionsMembers)) 
		return false;
	$userIds = explode(',', $WlmOptionsMembers[$wlmLevelId]);
	
	return in_array($userId, $userIds);
}

/**
 * Redirects to YH-VC Participants dashboard page 
 * user is logged in and has corresponding permission (level)
 * @return void
 */
function yhvc_participant_redirect()
{
	global $current_user;
	
	if (!is_user_logged_in())
		return;

	if ($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != 'index.php')
		return;

	if (userHasWlmLevel($current_user->ID, YH_VC_PARTICIPANTS_LEVEL))
	{
		header('Location: ' . site_url() . YH_VC_PARTICIPANTS_DASHBOARD);
		exit;
	}
}

?>