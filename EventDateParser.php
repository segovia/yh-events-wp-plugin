<?php

class EventDateParser
{
	/**
	 * Parses event date from format YYYY-MM-DD, for example 2012-01-30.
	 * Returns timestamp with hours, minutes and seconds set to 0.
	 * @static
	 * @param string $dateString
	 * @return int
	 */
	public static function parseEventDateFromCustomField($dateString)
	{
		if (preg_match('/(?P<y>\d{4})\s*?-\s*?(?P<m>\d{1,2})\s*?-\s*?(?P<d>\d{1,2})/is', $dateString, $m))
			return mktime(0, 0, 0, $m['m'], $m['d'], $m['y']);

		# By default use current date
		$date = getdate();
		return mktime(0, 0, 0, $date['mon'], $date['mday'], $date['year']);
	}

	/**
	 * Parses event time from format HH:MM, where hours are in 24-hour format.
	 * Returns number of seconds since the start of the day.
	 * @static
	 * @param string $timeString
	 * @return int
	 */
	public static function parseEventTimeFromCustomField($timeString)
	{
		if (preg_match('/(?P<h>\d{1,2})\s*:(?P<m>\d{1,2})/is', $timeString, $m))
			return ($m['h']*60 + $m['m'])*60;
		return 0;
	}

	function parseEventStartFromCustomFields($dateString, $timeString)
	{
		return self::parseEventDateFromCustomField($dateString) + self::parseEventTimeFromCustomField($timeString);
	}
}
